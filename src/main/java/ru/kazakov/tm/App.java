package ru.kazakov.tm;

import ru.kazakov.tm.controller.ProjectController;
import ru.kazakov.tm.controller.SystermController;
import ru.kazakov.tm.controller.TaskController;
import ru.kazakov.tm.controller.UserController;
import ru.kazakov.tm.enumerated.Role;
import ru.kazakov.tm.exception.ProjectNotFoundException;
import ru.kazakov.tm.exception.TaskNotFoundException;
import ru.kazakov.tm.repository.ProjectRepository;
import ru.kazakov.tm.repository.TaskRepository;
import ru.kazakov.tm.repository.UserRepository;
import ru.kazakov.tm.service.ProjectService;
import ru.kazakov.tm.service.ProjectTaskService;
import ru.kazakov.tm.service.TaskService;
import ru.kazakov.tm.service.UserService;

import java.util.Arrays;
import java.util.Scanner;

import static ru.kazakov.tm.constant.TerminalConst.*;

public class App {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final UserService userService = new UserService(userRepository, projectRepository, taskRepository);

    private final ProjectController projectController = new ProjectController(projectService, projectTaskService, userService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService, userService);

    private final UserController userController = new UserController(userService);

    private final SystermController systermController = new SystermController();

    {
        userRepository.create("ADMIN_AD1", "ADMIN1", "ADMIN2", "ADMIN3", "qwerty123", Role.ADMIN);
        userRepository.create("TEST_TT1", "TEST1", "TEST2", "TEST3", "345qwerty123", Role.USER);
        projectRepository.create("BPROJECT_1", "DESCRIPTION_PROJECT_1", userService.findByLogin("ADMIN_AD1").getId());
        projectRepository.create("APROJECT_2", "DESCRIPTION_PROJECT_1", userService.findByLogin("ADMIN_AD1").getId());
        taskRepository.create("BTASK_1", "DESCRIPTION_TASK_1", userService.findByLogin("ADMIN_AD1").getId());
        taskRepository.create("ATASK_2", "DESCRIPTION_TASK_1", userService.findByLogin("ADMIN_AD1").getId());
    }

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.systermController.displayWelcome();
        String command = "";
        while (!Exit.equals(command)) {
            command = scanner.nextLine();
            try {
                app.run(command);
            } catch (ProjectNotFoundException | TaskNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public int run(final String param) throws ProjectNotFoundException, TaskNotFoundException{
        if (param == null || param.isEmpty()) return -1;
        if (!Arrays.asList(ALL_ACTIONS).contains(param)) {
            if (userController.checkAuthorisation(param) == -1) return -1;
        }
        systermController.addCommandToHistory(param);
        switch (param) {
            case LOG_ON:
                return userController.registry();
            case LOG_OFF:
                return userController.logOff();
            case USER_INFO:
                return userController.displayUserInfo();
            case USER_UPDATE:
                return userController.updateUser();
            case USER_UPDATE_PASSWORD:
                return userController.updatePassword();

            case VERSION:
                return systermController.displayVersion();
            case ABOUT:
                return systermController.displayAbout();
            case HELP:
                return systermController.displayHelp();
            case Exit:
                return systermController.displayExit();
            case HISTORY:
                return systermController.displayHistory();

            case PROJECT_CREATE:
                return projectController.createProject();
            case PROJECT_CLEAR:
                return projectController.clearProject();
            case PROJECT_LIST:
                return projectController.listProject();
            case PROJECT_VIEW_BY_ID:
                return projectController.viewProjectById();
            case PROJECT_VIEW_BY_INDEX:
                return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_NAME:
                return projectController.viewProjectByName();
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return projectController.removeProjectByIndex();
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName();
            case PROJECT_UPDATE_BY_ID:
                return projectController.updateProjectById();
            case PROJECT_UPDATE_BY_INDEX:
                return projectController.updateProjectByIndex();

            case TASK_CREATE:
                return taskController.createTask();
            case TASK_CLEAR:
                return taskController.clearTask();
            case TASK_LIST:
                return taskController.listTask();
            case TASK_VIEW_BY_ID:
                return taskController.viewTaskById();
            case TASK_VIEW_BY_INDEX:
                return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_NAME:
                return taskController.viewTaskByName();
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById();
            case TASK_REMOVE_BY_INDEX:
                return taskController.removeTaskByIndex();
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName();
            case TASK_UPDATE_BY_ID:
                return taskController.updateTaskById();
            case TASK_UPDATE_BY_INDEX:
                return taskController.updateTaskByIndex();
            case TASK_LIST_BY_PROJECT_ID:
                return taskController.listTaskByProjectId();
            case TASK_ADD_PROJECT_BY_IDS:
                return taskController.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return taskController.removeTaskToProjectByIds();

            case USER_CREATE:
                return userController.createUser();
            case USER_CLEAR:
                return userController.clearUser();
            case USER_LIST:
                return userController.listUser();
            case USER_VIEW_BY_ID:
                return userController.viewUserById();
            case USER_VIEW_BY_INDEX:
                return userController.viewUserByIndex();
            case USER_VIEW_BY_LOGIN:
                return userController.viewUserByLogin();
            case USER_REMOVE_BY_ID:
                return userController.removeUserById();
            case USER_REMOVE_BY_INDEX:
                return userController.removeUserByIndex();
            case USER_REMOVE_BY_LOGIN:
                return userController.removeUserByLogin();
            case USER_UPDATE_BY_ID:
                return userController.updateUserById();
            case USER_UPDATE_BY_INDEX:
                return userController.updateUserByIndex();
            case USER_UPDATE_BY_LOGIN:
                return userController.updateUserByLogin();
            case USER_ASSIGN_PROJECT:
                return userController.addUsertoProjectbyIds();
            case USER_REMOVE_PROJECT:
                return userController.removeUsertoProjectbyIds();
            case USER_ASSIGN_TASK:
                return userController.addUsertoTaskbyIds();
            default:
                return systermController.displayError();
        }
    }
}
