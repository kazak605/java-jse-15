package ru.kazakov.tm.controller;

import ru.kazakov.tm.entity.Task;
import ru.kazakov.tm.exception.ProjectNotFoundException;
import ru.kazakov.tm.service.ProjectService;
import ru.kazakov.tm.entity.Project;
import ru.kazakov.tm.service.ProjectTaskService;
import ru.kazakov.tm.service.UserService;

import java.util.*;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    private final ProjectTaskService projectTaskService;

    private final UserService userService;

    public ProjectController(ProjectService projectService, ProjectTaskService projectTaskService,
                             UserService userService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createProject() throws ProjectNotFoundException {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
        final String description = scanner.nextLine();
        Project project = projectService.create(name, description, userService.currentUser.getId());
        if (project == null) {
            throw new ProjectNotFoundException("[CREATION FAILED!]");
        }
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT INDEX:]");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Long userId = userService.currentUser.getId();
            final Project project = projectService.findByIndex(index, userId);
            if (project == null) {
                throw new ProjectNotFoundException("[PROJECT IS NOT FOUND BY INDEX. UPDATE FAILED!");
            } else {
                System.out.println("[PLEASE, ENTER PROJECT NAME:]");
                final String name = scanner.nextLine();
                System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
                final String description = scanner.nextLine();
                projectService.update(project.getId(), name, description, userId);
                System.out.println("[OK]");
            }
        } catch (InputMismatchException | ProjectNotFoundException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int updateProjectById() throws ProjectNotFoundException {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final Long id = scanner.nextLong();
        final Long userId = userService.currentUser.getId();
        final Project project = projectService.findById(id, userId);
        if (project == null) {
            throw new ProjectNotFoundException("[PROJECT IS NOT FOUND BY ID. UPDATE FAILED!");
        } else {
            System.out.println("[PLEASE, ENTER PROJECT NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
            final String description = scanner.nextLine();
            projectService.update(project.getId(), name, description, userId);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        if (userService.currentUser == null) {
            projectService.clear();
            projectTaskService.clear();
        } else {
            for (Project project : projectService.findAllByUserId(userService.currentUser.getId())) {
                projectService.removeById(project.getId(), userService.currentUser.getId());
            }
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("[PLEASE, ENTER PROJECT INDEX:]");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Project project = projectService.removeByIndex(index, userService.currentUser.getId());
            if (project == null) {
                throw new ProjectNotFoundException("[PROJECT IS NOT FOUND. REMOVE FAILED!");
            } else {
                final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
                for (final Task task : tasks) {
                    projectTaskService.removeTaskFromProject(project.getId(), task.getId());
                }
                System.out.println("[OK]");
            }
        } catch (InputMismatchException | ProjectNotFoundException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int removeProjectByName() throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        final Long userId = userService.currentUser.getId();
        final List<Project> projects = projectService.removeByName(name, userId);
        if (projects == null) {
            throw new ProjectNotFoundException("[PROJECT IS NOT FOUND. REMOVE FAILED!");
        } else {
            for (Project project : projects) {
                final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
                for (final Task task : tasks) {
                    projectTaskService.removeTaskFromProject(project.getId(), task.getId());
                }
            }
            System.out.println("[OK]");
        }
        return 0;
    }

    public int removeProjectById() throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final Long id = scanner.nextLong();
        final Project project = projectService.removeById(id, userService.currentUser.getId());
        if (project == null) {
            throw new ProjectNotFoundException("[PROJECT IS NOT FOUND. REMOVE FAILED!");
        } else {
            final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
            System.out.println("[OK]");
        }
        return 0;
    }

    public void viewProject(final Project project) throws ProjectNotFoundException {
        if (project == null) {
            throw new ProjectNotFoundException("[PROJECTS IS NOT FOUND]");
        }
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("USER_ID: " + project.getUserId());
        System.out.println("[OK]");
    }

    public int viewProjectByIndex() {
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Project project = projectService.findByIndex(index, userService.currentUser.getId());
            viewProject(project);
        } catch (InputMismatchException | ProjectNotFoundException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int viewProjectByName() throws ProjectNotFoundException {
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        final List<Project> projects = projectService.findByName(name, userService.currentUser.getId());
        viewProjects(projects);
        return 0;
    }

    public int viewProjectById() throws ProjectNotFoundException {
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final long id = scanner.nextLong();
        final Project project = projectService.findById(id, userService.currentUser.getId());
        viewProject(project);
        return 0;
    }

    public int listProject() throws ProjectNotFoundException {
        System.out.println("[LIST PROJECT]");
        List<Project> projectList;
        if (userService.currentUser == null) {
            projectList = projectService.findAll();
        } else {
            projectList = projectService.findAllByUserId(userService.currentUser.getId());
        }
        viewProjects(projectList);
        return 0;
    }

    public void viewProjects(final List<Project> projects) throws ProjectNotFoundException {
        if (projects == null || projects.isEmpty()) {
            throw new ProjectNotFoundException("[PROJECTS ARE NOT FOUND]");
        }
        int index = 1;
        Collections.sort(projects, new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (final Project project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}
