package ru.kazakov.tm.controller;

import ru.kazakov.tm.entity.User;
import ru.kazakov.tm.enumerated.Role;
import ru.kazakov.tm.service.UserService;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class UserController extends AbstractController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public int createUser() {
        System.out.println("[PLEASE, CREATE USER]");
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        System.out.println("[PLEASE, ENTER FIRSTNAME:]");
        final String firstName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER MIDDLE NAME:]");
        final String middleName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER SURNAME:]");
        final String surName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER PASSWORD:]");
        String password = scanner.nextLine();
        if (userService.create(login, firstName, middleName, surName, password, Role.USER) == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateUserById() {
        System.out.println("[UPDATING A USER BY ID]");
        System.out.println("PLEASE ENTER, USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("PLEASE, ENTER LOGIN:");
            final String login = scanner.nextLine();
            System.out.println("PLEASE, ENTER FIRST NAME:");
            final String firstName = scanner.nextLine();
            System.out.println("PLEASE, ENTER MIDDLE NAME:");
            final String middleName = scanner.nextLine();
            System.out.println("PLEASE, ENTER SURNAME:");
            final String surName = scanner.nextLine();
            System.out.println("PLEASE, ENTER PASSWORD:");
            final String password = scanner.nextLine();
            System.out.println("PLEASE, ENTER ROLE:");
            final String role = scanner.nextLine();
            userService.update(user.getId(), login, firstName, middleName, surName, password, role);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateUserByIndex() {
        System.out.println("[UPDATING A USER BY INDEX]");
        System.out.println("PLEASE ENTER, USER INDEX:");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final User user = userService.findByIndex(index);
            if (user == null) {
                System.out.println("[FAIL]");
            } else {
                System.out.println("PLEASE, ENTER LOGIN:");
                final String login = scanner.nextLine();
                System.out.println("PLEASE, ENTER FIRST NAME:");
                final String firstName = scanner.nextLine();
                System.out.println("PLEASE, ENTER MIDDLE NAME:");
                final String middleName = scanner.nextLine();
                System.out.println("PLEASE, ENTER SURNAME:");
                final String surName = scanner.nextLine();
                System.out.println("PLEASE, ENTER PASSWORD:");
                final String password = scanner.nextLine();
                System.out.println("PLEASE, ENTER ROLE:");
                final String role = scanner.nextLine();
                userService.update(user.getId(), login, firstName, middleName, surName, password, role);
                System.out.println("[OK]");
            }
        } catch (InputMismatchException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int updateUserByLogin() {
        System.out.println("[UPDATING A USER BY LOGIN]");
        System.out.println("PLEASE ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER FIRST NAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLE NAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER SURNAME:");
        final String surName = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD:");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER ROLE:");
        final String role = scanner.nextLine();
        userService.update(user.getId(), login, firstName, middleName, surName, password, role);
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserById() {
        System.out.println("[REMOVE A USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.removeById(id);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeUserByIndex() {
        System.out.println("[REMOVE A USER BY ID]");
        System.out.println("PLEASE, ENTER USER INDEX:");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final User user = userService.removeByIndex(index);
            if (user == null) System.out.println("[FAIL]");
            else System.out.println("[OK]");
        } catch (InputMismatchException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearUser() {
        System.out.println("[USERS CLEAR]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("SURNAME: " + user.getSurName());
        System.out.println("PASSWORD: " + user.getPassword());
        System.out.println("PASSWORD HASH: " + user.getPasswordhash());
        System.out.println("ROLE: " + user.getRole());
        System.out.println("[OK]");
    }

    public void viewUsers(final List<User> users) {
        if (users == null || users.isEmpty()) {
            System.out.println("[USERS ARE NOT FOUND]");
            return;
        }
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user.getId() + ": " + user.getFirstName());
            index++;
        }
        System.out.println("[OK]");
    }

    public int listUser() {
        System.out.println("[USERS LIST]");
        viewUsers(userService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public int viewUserById() {
        System.out.println("ENTER, USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        viewUser(user);
        return 0;
    }

    public int viewUserByIndex() {
        System.out.println("ENTER, USER INDEX:");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final User user = userService.findByIndex(index);
            viewUser(user);
        } catch (InputMismatchException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int viewUserByLogin() {
        System.out.println("ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        viewUser(user);
        return 0;
    }

    public int checkAuthorisation(final String param) {
        if (!Arrays.asList(Role.ADMIN.getActions()).contains(param)) {
            System.out.println("WRONG OPERATION");
            return -1;
        }
        if (userService.currentUser == null) {
            System.out.println("PLEASE, LOG ON OR REGISTRY");
            return -1;
        }
        if (!Arrays.asList(userService.currentUser.getRole().getActions()).contains(param)) {
            System.out.println("NOT ALLOWED");
            return -1;
        }
        return 0;
    }

    public int registry() {
        System.out.println("[ENTER LOGIN:]");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("LOGIN IS NOT FOUND IN SYSTEM");
            return 0;
        }
        System.out.println("[ENTER LOGIN:]");
        if (userService.checkPassword(user, scanner.nextLine())) {
            System.out.println("WELCOME, " + user.getLogin());
            userService.currentUser = user;
        } else {
            System.out.println("FAIL");
        }
        return 0;
    }

    public int logOff() {
        userService.currentUser = null;
        System.out.println("LOG OFF");
        return 0;
    }

    public int displayUserInfo() {
        viewUser(userService.currentUser);
        return 0;
    }

    public int updateUser() {
        final String login;
        final User user;
        if (userService.currentUser == null) {
            System.out.println("FAIL");
        } else {
            if (userService.currentUser.getDisplayName().equals(Role.ADMIN.getDisplayName())) {
                System.out.println("[UPDATE INFORMATION OF ANY USER]");
                System.out.println("PLEASE ENTER, USER LOGIN:");
                login = scanner.nextLine();
                user = userService.findByLogin(login);
                if (user == null) {
                    System.out.println("[FAIL]");
                    return 0;
                }
            } else {
                System.out.println("[UPDATE INFORMATION OF CURRENT USER]");
                login = userService.currentUser.getLogin();
            }
            System.out.println("[ENTER PASSWORD]");
            final String password = scanner.nextLine();
            System.out.println("[ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[ENTER MIDDLE NAME:]");
            final String middleName = scanner.nextLine();
            System.out.println("[ENTER LAST NAME]");
            final String lastName = scanner.nextLine();
            userService.updateByLogin(login, firstName, middleName, lastName, password);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updatePassword() {
        final String login;
        final User user;
        if (userService.currentUser == null) {
            System.out.println("[FAIL]");
        } else {
            if (userService.currentUser.getDisplayName().equals(Role.ADMIN.getDisplayName())) {
                System.out.println("[UPDATE PASSWORD OF ANY USER]");
                System.out.println("PLEASE ENTER, USER LOGIN:");
                login = scanner.nextLine();
                user = userService.findByLogin(login);
                if (user == null) {
                    System.out.println("[FAIL]");
                    return 0;
                }
            } else {
                System.out.println("[UPDATE PASSWORD OF CURRENT USER]");
                login = userService.currentUser.getLogin();
                user = userService.currentUser;
            }
            System.out.println("[ENTER OLD PASSWORD:]");
            final String oldPassword = scanner.nextLine();
            if (userService.checkPassword(user, oldPassword)) {
                System.out.println("[ENTER NEW PASSWORD:]");
                final String newPassword = scanner.nextLine();
                userService.updatePasswordByLogin(login, newPassword);
                System.out.println("[OK]");
            } else {
                System.out.println("[WRONG PASSWORD]");
            }
        }
        return 0;
    }

    public int addUsertoProjectbyIds() {
        System.out.println("[ADD USER TO PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = scanner.nextLong();
        userService.addUserToProject(projectId, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int removeUsertoProjectbyIds() {
        System.out.println("[REMOVE USER FROM PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = scanner.nextLong();
        userService.removeUserFromProject(projectId, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int addUsertoTaskbyIds() {
        System.out.println("[ADD USER TO TASK BY IDS]");
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = scanner.nextLong();
        userService.addUserToTask(taskId, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int removeUsertoTaskbyIds() {
        System.out.println("[REMOVE USER FROM PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = scanner.nextLong();
        userService.removeUserFromTask(taskId, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;

    }
}
