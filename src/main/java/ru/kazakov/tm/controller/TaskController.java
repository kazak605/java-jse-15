package ru.kazakov.tm.controller;

import ru.kazakov.tm.entity.Task;
import ru.kazakov.tm.exception.ProjectNotFoundException;
import ru.kazakov.tm.exception.TaskNotFoundException;
import ru.kazakov.tm.service.ProjectTaskService;
import ru.kazakov.tm.service.TaskService;
import ru.kazakov.tm.service.UserService;

import java.util.*;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    private final UserService userService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService, UserService userService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createTask() throws TaskNotFoundException {
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        Task task = taskService.create(name, description, userService.currentUser.getId());
        if (task == null) {
            throw new TaskNotFoundException("[CREATION FAILED!]");
        }
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskById() throws ProjectNotFoundException {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEAS, ENTER TASK ID:]");
        final Long id = scanner.nextLong();
        final Long userId = userService.currentUser.getId();
        final Task task = taskService.findById(id, userId);
        if (task == null) {
            throw new ProjectNotFoundException("[TASK IS NOT FOUND BY ID. UPDATE FAILED!");
        } else {
            System.out.println("[PLEASE, ENTER TASK NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
            final String description = scanner.nextLine();
            taskService.update(task.getId(), name, description, userId);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateTaskByIndex() throws ProjectNotFoundException {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK INDEX:]");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Long userId = userService.currentUser.getId();
            final Task task = taskService.findByIndex(index, userId);
            if (task == null) {
                throw new ProjectNotFoundException("[TASK IS NOT FOUND BY ID. UPDATE FAILED!");
            } else {
                System.out.println("[PLEASE, ENTER TASK NAME:]");
                final String name = scanner.nextLine();
                System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
                final String description = scanner.nextLine();
                taskService.update(task.getId(), name, description, userId);
                System.out.println("[OK]");
            }
        } catch (InputMismatchException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int removeTaskById() throws ProjectNotFoundException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("[PLEASE, ENTER TASK ID:]");
        final long id = scanner.nextLong();
        final Long userId = userService.currentUser.getId();
        final Task task = taskService.removeById(id, userId);
        if (task == null) {
            throw new ProjectNotFoundException("[TASK IS NOT FOUND. REMOVE FAILED!");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX:");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Long userId = userService.currentUser.getId();
            final Task task = taskService.removeByIndex(index, userId);
            if (task == null) {
                throw new ProjectNotFoundException("[TASK IS NOT FOUND. REMOVE FAILED!");
            } else {
                System.out.println("[OK]");
            }
        } catch (InputMismatchException | ProjectNotFoundException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int removeTaskByName() throws ProjectNotFoundException {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        final Long userId = userService.currentUser.getId();
        final List<Task> tasks = taskService.removeByName(name, userId);
        if (tasks == null) {
            throw new ProjectNotFoundException("[TASK IS NOT FOUND. REMOVE FAILED!");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public void viewTask(final Task task) throws TaskNotFoundException {
        if (task == null) {
            throw new TaskNotFoundException("[TASKS ARE NOT FOUND]");
        }
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("USER_ID: " + task.getUserId());
        System.out.println("[OK]");
    }

    public int viewTaskById() throws TaskNotFoundException {
        System.out.println("ENTER, TASK ID:");
        final long id = scanner.nextLong();
        final Long userId = userService.currentUser.getId();
        final Task task = taskService.findById(id, userId);
        viewTask(task);
        return 0;
    }

    public int viewTaskByIndex() {
        System.out.println("[PLEASE, ENTER TASK INDEX:]");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Long userId = userService.currentUser.getId();
            final Task task = taskService.findByIndex(index, userId);
            viewTask(task);
        } catch (InputMismatchException | TaskNotFoundException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int viewTaskByName() throws TaskNotFoundException {
        System.out.println("[PLEASE, ENTER TASK NAME:]");
        final String name = scanner.nextLine();
        final Long userId = userService.currentUser.getId();
        final List<Task> tasks = taskService.findByName(name, userId);
        viewTasks(tasks);
        return 0;
    }

    public int listTask() throws TaskNotFoundException {
        System.out.println("[LIST TASK]");
        List<Task> taskList;
        if (userService.currentUser == null) {
            taskList = taskService.findAll();
        } else {
            taskList = taskService.findAllByUserId(userService.currentUser.getId());
        }
        viewTasks(taskList);
        return 0;
    }

    public void viewTasks(final List<Task> tasks) throws TaskNotFoundException {
        if (tasks == null || tasks.isEmpty()) {
            throw new TaskNotFoundException("[TASKS ARE NOT FOUND]");
        }
        int index = 1;
        Collections.sort(tasks, new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    public int listTaskByProjectId() throws TaskNotFoundException {
        System.out.println("[LIST TASKS BY PROJECT BY PROJECT ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        viewTasks(tasks);
        return 0;
    }

    public int addTaskToProjectByIds() throws TaskNotFoundException {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = Long.parseLong(scanner.nextLine());
        final Long userId = userService.currentUser.getId();
        Task task = projectTaskService.addTaskToProject(projectId, taskId, userId);
        if (task == null) {
            throw new TaskNotFoundException("[WRONG PROJECT ID OR TASK ID]");
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskToProjectByIds() throws TaskNotFoundException {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = Long.parseLong(scanner.nextLine());
        Task task = projectTaskService.removeTaskFromProject(projectId, taskId);
        if (task == null) {
            throw new TaskNotFoundException("[WRONG PROJECT ID OR TASK ID]");
        }
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        if (userService.currentUser == null) {
            taskService.clear();
            projectTaskService.clear();
        } else {
            for (Task task : taskService.findAllByUserId(userService.currentUser.getId())) {
                taskService.removeById(task.getId(), userService.currentUser.getId());
            }
        }
        System.out.println("[OK]");
        return 0;
    }

}
