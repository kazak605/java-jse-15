package ru.kazakov.tm.service;

import ru.kazakov.tm.entity.Project;
import ru.kazakov.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return projectRepository.create(name, description, userId);
    }

    public Project update(final Long id, final String name, final String description, final Long userId) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description, userId);
    }

    public Project findById(final Long id, final Long userId) {
        if (id == null) return null;
        return projectRepository.findById(id, userId);
    }

    public Project findByIndex(final int index, final Long userId) {
       // if (index < 0 || index > projectRepository.findAll().size() - 1) return null;
        return projectRepository.findByIndex(index, userId);
    }

    public List<Project> findByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name, userId);
    }

    public Project removeById(final Long id, final Long userId) {
        if (id == null) return null;
        return projectRepository.removeById(id, userId);
    }

    public Project removeByIndex(final int index, final Long userId) {
        if (index < 0 || index > projectRepository.findAll().size() - 1) return null;
        return projectRepository.removeByIndex(index, userId);
    }

    public List<Project> removeByName(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name, userId);
    }

    public void clear() {
        projectRepository.clear();
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAllByUserId(Long userId) {
        if (userId == null) return null;
        return projectRepository.findAllByUserId(userId);
    }

}
