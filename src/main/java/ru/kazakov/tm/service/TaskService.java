package ru.kazakov.tm.service;

import ru.kazakov.tm.entity.Task;
import ru.kazakov.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    public Task create(final String name, final String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return  null;
        return taskRepository.create(name, description, userId);
    }

    public Task update(final Long id, final String name, final String description, final Long userId) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return  null;
        return taskRepository.update(id, name, description, userId);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findById(final Long id, final Long userId) {
        if (id == null) return null;
        if (userId == null) return  null;
        return taskRepository.findById(id, userId);
    }

    public Task findByIndex(final int index, final Long userId) {
        if (index < 0 || index > taskRepository.findAll().size() - 1) return null;
        if (userId == null) return  null;
        return taskRepository.findByIndex(index, userId);
    }

    public List<Task> findByName(final String name,final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return taskRepository.findByName(name, userId);
    }

    public List<Task> findAllByUserId(final Long userId) {
        if (userId == null) return null;
        return taskRepository.findAllByUserId(userId);
    }

    public Task removeById(final Long id,final Long userId) {
        if (id == null) return null;
        if (userId == null) return null;
        return taskRepository.removeById(id, userId);
    }

    public Task removeByIndex(final int index,final Long userId) {
        if (index < 0 || index > taskRepository.findAll().size() - 1) return null;
        if (userId == null) return null;
        return taskRepository.removeByIndex(index, userId);
    }

    public List<Task> removeByName(final String name,final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name, userId);
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null ) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (projectId == null) return null;
        if (id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    public void clear() {
        taskRepository.clear();
    }

}
