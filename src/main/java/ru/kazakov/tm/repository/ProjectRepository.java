package ru.kazakov.tm.repository;

import ru.kazakov.tm.entity.Project;
import ru.kazakov.tm.entity.Task;
import ru.kazakov.tm.entity.User;
import ru.kazakov.tm.exception.ProjectNotFoundException;

import java.beans.PropertyEditorSupport;
import java.util.*;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();

    private HashMap<String, List<Project>> projectAssoc = new HashMap<>();

    public Project addToMap(final Project project) {
        List<Project> projectsInMap = projectAssoc.get(project.getName());
        if (projectsInMap == null) projectsInMap = new ArrayList<>();
        projectsInMap.add(project);
        projectAssoc.put(project.getName(), projectsInMap);
        return project;
    }

    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        addToMap(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.add(project);
        addToMap(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId) {
        final Project project = new Project(name, description, userId);
        projects.add(project);
        addToMap(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description, final Long userId) {
        Project project = findById(id, userId);
        if (project == null) return null;
        String oldName = project.getName();
        List<Project> projectListOld = findByName(oldName, userId);
        if (projectListOld == null) return null;
        project.setName(name);
        project.setDescription(description);
        if (!oldName.equals(name) && projectListOld.size() > 1) {
            List<Project> projectListNew = new ArrayList<>();
            projectListNew.add(project);
            projectListOld.remove(project);
            projectAssoc.remove(oldName);
            projectAssoc.put(oldName, projectListOld);
            projectAssoc.put(name, projectListNew);
        } else {
            projectAssoc.remove(oldName);
            projectAssoc.put(name, projectListOld);
        }
        return project;
    }

    public Project findById(final Long id, final Long userId) {
        try {
            List<Project> currentListProject;
            if (userId == null) {
                currentListProject = findAll();
            } else {
                currentListProject = findAllByUserId(userId);
            }
            if (currentListProject == null || currentListProject.size() == 0) {
                throw new ProjectNotFoundException("Project's not found by id = " + id);
            }
            for (final Project project : currentListProject) {
                if (project.getId().equals(id)) return project;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Project findByIndex(int index, final Long userId) {
        try {
            List<Project> result;
            if (userId == null) {
                result = findAll();
            } else {
                result = findAllByUserId(userId);
            }
            if (result == null || result.size() == 0) {
                throw new ProjectNotFoundException("Project's not found by index = " + index);
            }
            if (index < 0 || index > result.size() - 1) {
                throw new ProjectNotFoundException("Wrong index");
            }
            return result.get(index);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Project> findByName(final String name, final Long userId) {
        try {
            if (!projectAssoc.containsKey(name)) throw new ProjectNotFoundException("Projects're not found by name" + name);
            List<Project> result = new ArrayList<>();
            if (userId == null) {
                result = projectAssoc.get(name);
            } else {
                for (Project project : projectAssoc.get(name)) {
                    if (project.getUserId().equals(userId)) {
                        result.add(project);
                    }
                }
            }
            if (result == null || result.size() == 0) {
                throw new ProjectNotFoundException("Projects're not found by name " + name);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Project removeByIndex(final int index, final Long userId) {
        Project project = findByIndex(index, userId);
        if (project == null) return null;
        List<Project> projectFromName = findByName(project.getName(), userId);
        projectAssoc.remove(project.getName());
        projectFromName.remove(project);
        projectAssoc.put(project.getName(), projectFromName);
        return project;
    }

    public Project removeById(final Long id, final Long userId) {
        Project project = findById(id, userId);
        if (project == null) return null;
        List<Project> projectFromName = findByName(project.getName(), userId);
        projectAssoc.remove(project.getName());
        projectFromName.remove(project);
        projectAssoc.put(project.getName(), projectFromName);
        return project;
    }

    public List<Project> removeByName(final String name, final Long userId) {
        List<Project> projectList = findByName(name, userId);
        if (projectList == null || projectList.size() == 0) return null;
        projects.removeAll(projectList);
        projectAssoc.remove(name);
        return projectList;
    }

    public void clear() {
        projects.clear();
        projectAssoc.clear();
    }

    public List<Project> findAll() {
        return projects;
    }

    public List<Project> findAllByUserId(final Long userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : findAll()) {
            final Long IdUser = project.getUserId();
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(project);
        }
        return result;
    }

}
